import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:medx_mart/home.dart';
import 'dart:ui' as color;

import 'package:medx_mart/splash_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white,
        /* set Status bar color in Android devices. */
        statusBarIconBrightness: Brightness.dark,
        /* set Status bar icons color in Android devices.*/
        systemNavigationBarColor: Colors.transparent,
        /* set Status bar icons color in Android devices.*/
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.dark));
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light));
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: kPrimaryColor,
        ),
        home: Splash(),
        routes: {
          HomePageWidget.routeName: (_) => HomePageWidget(),
        });
  }
}

const green = color.Color(0xff117660);
const darkColor = color.Color(0xff1a1a1a);
const redColor = color.Color(0xffce0a36);
const MaterialColor kPrimaryColor = const MaterialColor(
  0xFF117660,
  const <int, Color>{
    50: green,
    100: green,
    200: green,
    300: green,
    400: green,
    500: green,
    600: green,
    700: green,
    800: green,
    900: green,
  },
);
