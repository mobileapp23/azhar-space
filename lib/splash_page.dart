import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medx_mart/home.dart';
import 'package:medx_mart/main.dart';

class Splash extends StatelessWidget {
  static const routeName = '/splash';

  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MainContainer(),
    );
  }
}

class MainContainer extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<MainContainer> {
  late double _height, _width;
  @override
  void initState() {
    super.initState();
    new Future.delayed(
      const Duration(seconds: 3),
      () => Navigator.pushReplacementNamed(context, HomePageWidget.routeName),
    );
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return Scaffold(
        backgroundColor: darkColor,
        extendBodyBehindAppBar: true,
        body: Material(
          type: MaterialType.transparency,
          elevation: 10.0,
          child: Center(
            child: Image.asset(
              'assets/logos/azhar_space_logo.png',
              height: _height * 0.4,
              width: _width * 0.8,
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.all(8.0),
          margin: EdgeInsets.only(bottom: 0),
          color: darkColor,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Powered by",
                style: GoogleFonts.lato(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 16.0),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Material(
                    type: MaterialType.transparency,
                    elevation: 10.0,
                    child: Image.asset(
                      'assets/logos/trapo_white_logo.png',
                      height: _height * 0.04,
                      width: _width * 0.6,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
